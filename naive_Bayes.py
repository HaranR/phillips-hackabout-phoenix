import os.path
import numpy as np
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import LSTM, SimpleRNN
from keras.layers import Dropout
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.models import model_from_json
from input import load_data
from keras.utils import to_categorical
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras import optimizers
from keras.layers.pooling import AveragePooling1D
from keras.layers.wrappers import Bidirectional
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics    import accuracy_score

model_path = 'model/keras_initial_model.json'
train_file_path = 'datasets/TRAIN.txt'
test_file_path = 'datasets/TEST.txt'
df_train, df_test, le= load_data(train_file_path, test_file_path, "|")
print le.classes_
print df_train.head()
print df_test.head()
X_train, y_train = df_train.X, df_train.Y
X_test, y_test = df_test.X, df_test.Y

max_review_length = 100
X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)
from sklearn.svm import SVC as mod
clf = mod()
clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)
print accuracy_score(y_test,y_pred)


