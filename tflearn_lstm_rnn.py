# LSTM with dropout for sequence classification in the IMDB dataset
import numpy
from input import load_data
import tflearn
from tflearn.data_utils import to_categorical,pad_sequences

# fix random seed for reproducibility
numpy.random.seed(7)
# load the dataset but only keep the top n words, zero the rest
top_words=5000
df=load_data(
	'/home/rajnunes/Projects/phillips-hackabout-phoenix/datasets/SemEval2010_task8_training/MOD2_TRAIN_FILE .TXT',"|")
msk=numpy.random.rand(len(df))<0.8

X_train,y_train=df[msk].X,df[msk].Y
X_test,y_test=df[~msk].X,df[~msk].Y
X_train=numpy.array(X_train)

y_train=numpy.array(y_train)
print len(X_train)
print len(y_train)
X_test=numpy.array(X_test)
y_test=numpy.array(y_test)

#(X_train,y_train),(X_test,y_test)=imdb.load_data(path="/home/rajnunes/Projects/Philips/imdb.npz",
#												 num_words=top_words)
# print imdb.get_word_index()
# skip_top=0,
# maxlen=None,
# seed=113,
# start_char=1,
# oov_char=2,
# index_from=3)
# imdb.load_data(num_words=top_words)
# truncate and pad input sequences
# print type(X_train[0])
# X_train=numpy.array(X_train)

max_review_length=100
X_train=pad_sequences(X_train,maxlen=max_review_length)
X_test=pad_sequences(X_test,maxlen=max_review_length)
# print len(X_train[0])
# print len(X_train[1])
# create the model
# X_train = X_train.reshape[1.len(X_train[0])]
net=tflearn.input_data([None,100])
net=tflearn.embedding(net,input_dim=100,output_dim=128)
net=tflearn.lstm(net,128,dropout=0.8)
net=tflearn.fully_connected(net,10,activation='softmax')
net=tflearn.regression(net,optimizer='adam',learning_rate=0.001,
					   loss='categorical_crossentropy')
model=tflearn.DNN(net)
# model.fit()
model.fit(X_train,y_train,validation_set=(X_test,y_test),show_metric=True,batch_size=64)
