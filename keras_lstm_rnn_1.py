import os.path
import numpy
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.models import model_from_json
from input import load_data
from keras.utils import to_categorical
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers.convolutional import UpSampling1D

model_path = 'model/keras_initial_model.json'
train_file_path = 'datasets/TRAIN.txt'
test_file_path = 'datasets/TEST.txt'
df_train, df_test = load_data(train_file_path, test_file_path, "|")

X_train, y_train = df_train.X, df_train.Y
X_test, y_test = df_test.X, df_test.Y

X_train = numpy.array(X_train)
y_train = numpy.array(y_train)

y_train = numpy.array(y_train)
y_test = numpy.array(y_test)

y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

max_review_length = 100
X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)

# If file exists, load model and weights from file else create the model
if os.path.isfile(model_path):
    json_file = open('model/keras_initial_model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    model.load_weights("model/keras_initial_model.h5")
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print("Loaded model from disk")

else:
    embedding_vecor_length = 32
    model = Sequential()
    model.add(Embedding(88585, embedding_vecor_length, input_length=max_review_length))
    model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
    model.add(MaxPooling1D(pool_size=2))

    # model.add(LSTM(32))
    model.add(LSTM(32, dropout=0.4, recurrent_dropout=0.5))

    # model.add(LSTM(16, dropout=0.4, recurrent_dropout=0.5,return_sequences=True))

    # model.add(LSTM(16,dropout=0.4, recurrent_dropout=0.5))
    # model.add(UpSampling1D(size=2))
    # model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
    # model.add(UpSampling1D(size=2))
    model.add(Dense(10, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print("Created model")

print(model.summary())
model.fit(X_train, y_train, epochs=20, batch_size=800)

# Evaluation of the model
scores = model.evaluate(X_test, y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1] * 100))

prompt = raw_input("Save model?(y/n)")
if prompt == "y" or prompt == "Y":
    model_json = model.to_json()
    # serialize model to JSON
    with open("model/keras_initial_model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model/keras_initial_model.h5")
    print("Saved model to disk")
