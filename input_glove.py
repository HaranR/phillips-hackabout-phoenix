import pandas as pd
from keras.preprocessing.text import Tokenizer
import numpy as np
import json
from sklearn import preprocessing


def load_data(path_train, path_test, delim):
    df_train = pd.read_csv(path_train, header=None, delimiter=delim, names=["X", "Y"])
    df_test = pd.read_csv(path_test, header=None, delimiter=delim, names=["X", "Y"])
    tokenizer = Tokenizer(nb_words=None)
    tokenizer.fit_on_texts(df_train.X)
    df_train.X = tokenizer.texts_to_sequences(df_train.X)
    df_test.X = tokenizer.texts_to_sequences(df_test.X)
    word_index = tokenizer.word_index
    embeddings_index = {}
    with open('datasets/glove.840B.300d.txt', 'r') as f:
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs

    le = preprocessing.LabelEncoder()
    le.fit(df_train['Y'])

    EMBEDDING_DIM = 300
    embedding_matrix = np.zeros((len(word_index) + 1, EMBEDDING_DIM))
    for word, i in word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # words not found in embedding index will be all-zeros.
            embedding_matrix[i] = embedding_vector

    df_train['Y'] = le.transform(df_train['Y'])
    df_test['Y'] = le.transform(df_test['Y'])
    return df_train, df_test, le, embedding_matrix,word_index

