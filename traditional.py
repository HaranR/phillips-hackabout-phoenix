import numpy as np
from input import load_data
#import tflearn
from sklearn.naive_bayes import GaussianNB
#from tflearn.data_utils import to_categorical, pad_sequences
#from keras.preprocessing.sequence import pad_sequences
max_review_words=5000
df = load_data('/home/hr/Phillips/phillips-hackabout-phoenix/datasets/SemEval2010_task8_training/MOD2_TRAIN_FILE .TXT',"|")
msk = np.random.rand(len(df)) < 0.8

def classifier():
	clf = GaussianNB()
	return clf

X_train,y_train = df[msk].X, df[msk].Y
X_test,y_test = df[~msk].X,df[~msk].Y

max_len = max(map(lambda x:len(x),df.X))

for i in X_train :
	if len(i)!= max_len:
		i+=[0]*(max_len-len(i))

for i in X_test 	:
	if len(i)!= max_len:
		i+=[0]*(max_len-len(i))

#X_train=pad_sequences(X_train,maxlen=max_review_length)
#X_test=pad_sequences(X_test,maxlen=max_review_length)

X_train = np.array(X_train)

X_test = np.array(X_test)

clf = classifier()
clf.fit( X_train,y_train )
Y_pred = clf.predict(X_test)
from sklearn.metrics import accuracy_score

print accuracy_score(y_test,Y_pred)


