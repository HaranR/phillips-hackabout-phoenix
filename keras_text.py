from keras.models import Sequential, Model
from keras.layers import Activation, Dense, Dropout, Embedding, Flatten, Input, Merge, Convolution1D, MaxPooling1D
from keras import backend as K
embedding_dim = 100
filter_sizes = (3, 4)
num_filters = 150
dropout_prob = (0.25, 0.5)
hidden_dims = 150

K.set_session(sess)
model_path = 'model/keras_initial_model.json'
train_file_path = 'datasets/TRAIN.txt'
test_file_path = 'datasets/TEST.txt'
df_train, df_test = load_data(train_file_path, test_file_path, "|")

X_train, y_train = df_train.X, df_train.Y
X_test, y_test = df_test.X, df_test.Y

X_train = numpy.array(X_train)
y_train = numpy.array(y_train)

y_train = numpy.array(y_train)
y_test = numpy.array(y_test)

y_train = to_categorical(y_train)
y_test = to_categorical(y_test)