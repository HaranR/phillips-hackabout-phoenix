import os.path
import numpy

import keras
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import LSTM, SimpleRNN
from keras.layers import Dropout
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.models import model_from_json
from input_glove import load_data
from keras.utils import to_categorical
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras import optimizers
from keras.layers import Activation, LSTM, Merge
from keras.layers.pooling import AveragePooling1D
from keras.layers.wrappers import Bidirectional

model_path = 'model/keras_initial_model.json'
train_file_path = 'datasets/TRAIN_FILE_1.txt'
test_file_path = 'datasets/TEST_FILE_FULL_1.txt'
df_train, df_test, le, embedding_matrix, word_index = load_data(train_file_path, test_file_path, "|")
print le.classes_
print df_train.head()
print df_test.head()
X_train, y_train = df_train.X, df_train.Y
X_test, y_test = df_test.X, df_test.Y

X_train = numpy.array(X_train)
y_train = numpy.array(y_train)

y_train = numpy.array(y_train)
y_test = numpy.array(y_test)

y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

max_review_length = 100
X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)

# If file exists, load model and weights from file else create the model
if os.path.isfile(model_path):
    json_file = open('model/keras_initial_model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    model.load_weights("model/keras_initial_model.h5")
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print("Loaded model from disk")

else:
    embedding_vecor_length = 300
    # model_f = Sequential()
    # model_b = Sequential()

    # model_f.add(Conv1D(filters=64, kernel_size=5, padding='same', activation='relu'))
    # model_f.add(MaxPooling1D(pool_size=2))
    #
    # model_b.add(Embedding(len(word_index) + 1, embedding_vecor_length, weights=[embedding_matrix],
    #                     input_length=max_review_length, trainable=False))
    # model_b.add(Conv1D(filters=64, kernel_size=5, padding='same', activation='relu'))
    # model_b.add(MaxPooling1D(pool_size=2))
    # model_f.add(Bidirectional(LSTM(16,activation='relu',dropout=0.4,recurrent_dropout=0.6)))
    # model_b.add(Bidirectional(LSTM(16,activation='relu',dropout=0.4,recurrent_dropout=0.6)))
    # model = Sequential()
    # model.add(Merge([model_f, model_b], mode='concat', concat_axis=1))
    # model.add(Dropout(0.1))
    model = Sequential()
    model.add(Embedding(len(word_index) + 1, embedding_vecor_length, weights=[embedding_matrix],
                        input_length=max_review_length, trainable=False))
    model.add(Conv1D(filters=32, kernel_size=4, padding='same', activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    # model.add(Conv1D(filters=32, kernel_size=4, padding='same', activation='relu'))
    # model.add(MaxPooling1D(pool_size=2))


    # model.add(LSTM(32, dropout=0.4, recurrent_dropout=0.6,return_sequences=True))
    model.add(LSTM(32, dropout=0.4, recurrent_dropout=0.6))
    model.add(Dropout(0.5))
    #
    # model.add(Conv1D(filters=8, kernel_size=3, padding='same', activation='relu'))
    # model.add(MaxPooling1D(pool_size=2))
    #
    # model.add(Bidirectional(LSTM(4, dropout=0.5, recurrent_dropout=0.8)))
    # model.add(Dropout(0.5))

    # lstm_b = LSTM(16,activation='relu',dropout=0.4,recurrent_dropout=0.6)
    # model.add(Bidirectional(lstm,backward = lstm_b))
    # model.add(Flatten())
    # model.add(LSTM(8, activation='lrelu', dropout=0.5, recurrent_dropout=0.55))
    # model.add(Dense(32, activation='softmax'))
    # model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
    # model.add(MaxPooling1D(pool_size=2))

    # model.add(Bidirectional(SimpleRNN(64, activation='relu', recurrent_dropout=0.4)))
    # model.add(Flatten())
    # model.add(Bidirectional(SimpleRNN(64, dropout=0.2, recurrent_dropout=0.2, return_sequences=True)))
    # model.add(Bidirectional(SimpleRNN(64, dropout=0.2, recurrent_dropout=0.2, return_sequences=True)))
    # model.add(Bidirectional(SimpleRNN(64, dropout=0.2, recurrent_dropout=0.2, return_sequences=True)))
    # model.add(Bidirectional(SimpleRNN(64, dropout=0.2, recurrent_dropout=0.2, return_sequences=True)))
    # model.add(Bidirectional(SimpleRNN(64, dropout=0.2, recurrent_dropout=0.2, return_sequences=True)))
    # model.add(SimpleRNN(64, activation='relu', recurrent_dropout=0.4))

    # model.add(LSTM(32, dropout=0.2, recurrent_dropout=0.2,return_sequences=True,unit_forget_bias=True))
    # model.add(Bidirectional(LSTM(32,dropout=0.4,recurrent_dropout=0.6,return_sequences=True,unit_forget_bias=True, input_shape=(5, 10))))
    # model.add(Bidirectional(LSTM(32,dropout=0.4,recurrent_dropout=0.6, unit_forget_bias=True)))
    # model.add(Bidirectional(LSTM(32,dropout=0.2,recurrent_dropout=0.2,return_sequences=True,unit_forget_bias=True)))
    # model.add(LSTM(32,dropout=0.2,recurrent_dropout=0.2))
    # # model.add(Dense(128, activation='sigmoid'))
    model.add(Dense(10, activation='sigmoid'))
    nad = keras.optimizers.Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004)
    model.compile(loss='categorical_crossentropy', optimizer=nad, metrics=['accuracy'])
    print("Created model")

print(model.summary())
model.fit(X_train, y_train, epochs=40, batch_size=800)

# Evaluation of the model

while True:
    scores = model.evaluate(X_test, y_test, verbose=0)
    print("Accuracy: %.2f%%" % (scores[1] * 100))
    prompt = raw_input("Want to continue?(y/n)")
    if prompt == "y" or prompt == "Y":
        num = int(raw_input("Enter epochs : "))
        model.fit(X_train, y_train, epochs=num, batch_size=800)
    else:
        break
prompt = raw_input("Save model?(y/n)")
if prompt == "y" or prompt == "Y":
    model_json = model.to_json()
    # serialize model to JSON
    with open("model/keras_initial_model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model/keras_initial_model.h5")
    print("Saved model to disk")
