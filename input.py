import pandas as pd
import json
from sklearn import preprocessing


def load_data(path_train, path_test, delim):
    df_train = pd.read_csv(path_train, header=None, delimiter=delim, names=["X", "Y"])
    df_test = pd.read_csv(path_test, header=None, delimiter=delim, names=["X", "Y"])

    json1_file = open('datasets/word_map.json')
    json1_str = json1_file.read()
    word_map = json.loads(json1_str)
    le = preprocessing.LabelEncoder()
    le.fit(df_train['Y'])

    X = []
    for line in df_train.X:
        vec = []
        for word in line.split():
            word = word.lower()
            word = word_map.get(word, 0)
            vec.append(word)
        X.append(vec)
    df_train['X'] = X

    X = []
    for line in df_test.X:
        vec = []
        for word in line.split():
            word = word.lower()
            word = word_map.get(word, 0)
            vec.append(word)
        X.append(vec)
    df_test['X'] = X

    df_train['Y'] = le.transform(df_train['Y'])
    df_test['Y'] = le.transform(df_test['Y'])
    return df_train, df_test, le
